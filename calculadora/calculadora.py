#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 09:48:09 2019

author: Pavel E. Vázquez Mtz.
Transcripcion del codigo impreso prororcionado en el 
curso de Python en Pilatres Benita galeana. 

Calculadora con la interfaz gráfica tkinter
"""

from tkinter import *
from math import *

#####################
# Ventana Principal
#####################
ventana = Tk()
ventana.title("Calcladora")
ventana.geometry("392x600")
ventana.configure(background="SkyBlue4")

#######################################
# Variables para estilo de los botones
# ancho, alto y color
#######################################
color_boton=("gray77")
ancho_boton = 8
alto_boton = 3

# Variables para desplegar texto 
input_text = StringVar()
operador = ""

#######################################
# Declaracion y definicion de funciones
#######################################

def clear():
    global operador
    print('Clear')
    operador = ("")
    input_text.set("0")
    

def btnClick(num):
    global operador
    operador = operador + str(num)
    print(operador)
    input_text.set(operador)

def operacion():
    global operador
    try:
        opera=str(eval(operador))
    except:
        clear()
        opera = ('Error')
    input_text.set(opera)
    
###################################
# Declaración de botones del 0 al 9
###################################
Boton0 = Button(ventana, text = "0", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("0")).place(x = 17, y = 180)
Boton1 = Button(ventana, text = "1", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("1")).place(x = 107, y = 180)
Boton2 = Button(ventana, text = "2", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("2")).place(x = 197, y = 180)
Boton3 = Button(ventana, text = "3", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("3")).place(x = 287, y = 180)
Boton4 = Button(ventana, text = "4", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("4")).place(x = 17, y = 240)
Boton5 = Button(ventana, text = "5", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("5")).place(x = 107, y = 240)
Boton6 = Button(ventana, text = "6", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("6")).place(x = 197, y = 240)
Boton7 = Button(ventana, text = "7", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("7")).place(x = 287, y = 240)
Boton8 = Button(ventana, text = "8", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("8")).place(x = 17, y = 300)
Boton9 = Button(ventana, text = "9", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("9")).place(x = 107, y = 300)

#####################################################
# Declaración de botones de operaciones suma, resta, 
# multiplicación, División, raiz cuadrada, residuo,
# ln, Exp 
#####################################################
BotonSuma = Button(ventana, text = "+", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("+")).place(x = 17, y = 360)
BotonResta = Button(ventana, text = "-", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("-")).place(x = 107, y = 360)
BotonMulti = Button(ventana, text = "*", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("*")).place(x = 197, y = 360)
BotonDiv = Button(ventana, text = "/", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("/")).place(x = 287, y = 360)
BotonSqrt = Button(ventana, text = "sqrt", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("sqrt")).place(x = 17, y = 420)
BotonResto = Button(ventana, text = "%", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("%")).place(x = 197, y = 480)
BotonLn = Button(ventana, text = "ln", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("log")).place(x = 287, y = 480)
BotonExp = Button(ventana, text = "EXP", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("**")).place(x = 197, y = 420)

#####################################################
# Declaración de botones de agrupacion () 
# ###################################################
BotonParen1 = Button(ventana, text = "(", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("(")).place(x = 17, y = 480)
BotonParen2 = Button(ventana, text = ")", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick(")")).place(x = 107, y = 480)

##########################
# Declaración de boton Pi
# ########################
BotonPi = Button(ventana, text = "pi", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = lambda : btnClick("pi")).place(x = 197, y = 300)

##########################
# Declaración de boton Pi
# ########################
BotonC = Button(ventana, text = "C", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = clear).place(x = 107, y = 420)

####################################
# Declaración de boton de resultado
####################################
BotonC = Button(ventana, text = "=", bg = color_boton, width = ancho_boton,
  height = alto_boton, command = operacion).place(x = 287, y = 420)


# Salida
Salida = Entry(ventana, font = ('arial', 20, 'bold'), width = 22, textvariable = input_text, bd = 20, insertwidth = 4, bg = "powder blue", justify = "right").place(x = 10, y= 60)



ventana.mainloop()
