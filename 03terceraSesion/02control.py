# -*- coding: utf-8 -*-
"""
Apuntes tercera sesion curso de Pyton
en Pilares benita Galeana

@author: Pavel E Vazquez Mtz
"""

#%% Ejercicio 1

# Si semáforo esta en verde cruzar la calle, si no espere

if 'semaforo' == 'verde':
    print('Cruzar la calle')
else:
    print('esperar')
        
#%% Ejercicio 2

    
#    Si gasto hasta  $100 pago con efectivo 
#    si no, si gasto mas de 100 pesos y menos de 300 pago con targeta de debito
#    Si no, pago con targeta de credito

 
compra = 300
 
if compra <= 100:
    print('Pago con efectivo')
elif compra > 100 and compra < 300:
    print('Pago con tarjeta de debito')
else:
    print('Pago con tarjeta de credito')    
     
#%% Ejercicio 3
    
    # Si una compra es mayor a $100 obtengo un descuento de 10%
    
total_compra=110
tasa_descuento = 10

print(total_compra)
print(tasa_descuento)

if total_compra > 100:
    importe_descuento = total_compra * tasa_descuento / 100
    print(importe_descuento)
    importe_a_pagar = total_compra - importe_descuento
    print(importe_a_pagar)
    
#%% while
    
#    Mientras el año sea menor o igual a 2012 imprimir la frase "Informes del año"
    
anio=2001

while anio <= 2012:
    print('Informes del año:', str(anio))
    anio += 1
    
#%% For Por cada nombre en mi lista imprimir el nombre

import time
    
mi_lista = ['Jesus', 'Gustavo', 'Rocio', 'Edith']

for nombre in mi_lista:
    time.sleep(3)
    print(nombre)
    
#%% Ejercicio 2
"""    
Realizar un programa que lea dos numeros por teclado y permita elegir
entre tres opciones en un menu:
    Mostrar la suma
    Mostrar la resta
    mostrar una multiplicación
    En caso de introducir una opción invalida el programa informara que no es correcta
"""
print('Suma, resta o multiplica dos numeros')
num1 = float(input('Escribe el primer numero: '))
num2 = float(input('Escribe el segundo numero: '))

#print('(s)uma, (r)esta o (m)ultiplicacion: ')
operacion = input('(s)uma, (r)esta o (m)ultiplicacion: ')
if operacion == 's' :
    print(num1, '+', num2, '=', num1 + num2)
elif operacion == 'r' :
    print(num1, '-', num2, '=', num1 - num2)
elif operacion == 'm' :
    print(num1, '*', num2, '=', num1 * num2)
else :
    print('opcion incorrecta')
    

#%% Ejercicio 3
    
    """ Realiza un programa que sume todos los números enteros pares desde
    el 0 hasta el 100
    """
    
    
