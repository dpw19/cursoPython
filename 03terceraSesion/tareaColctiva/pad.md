% Ejercicio colaborativo 01
% Curos de Python
% 25/06/2019


Pad paythonpilares
===================

** Creación: ** 19/06/2019

** Propósito: ** Actividad colectiva para solucionar un probleema de programación planteado
en la tercera sesión del curso de Python en el PILARES Benita Galeana

** Fuente: ** https://pad.riseup.net/p/pythonPilares

Problema a resolver (Estrella , Pavel, )
=======================================

Realiza un programa que pida al usuario cuántos números desea introducir. 
Luego lee todos los números y realiza una media aritmética.

Si utilizamos lenguaje incluyente puede quedar así:

Realiza un programa que pida cuántos números se desean introducir. 
Luego lee todos los números y realiza una media aritmética.

Es importante utilizar lenguaje incluyente para que las personas tanto del
género femenino como las de género masculino se sientan nombradas, a diferencia
de lo que plantea la RAE sobre el genérico masculino que pretende que se adivine
cuándo este se refiere a las personas en general y cuándo a 
los hombres. Más inforación en Guichard Bello, Claudia "Manual de comunicación
no sexista, hacia un lenguaje incluyente".

Bloques del plograma:
=====================
Lectura de cantidad de números (Pavel,)
----------------------------------------

### Descripción del bloque

Escribe el mensaje *¿Cuántos números deseas capturar?:* Y guarda en la variable *longitud* un valor entero.
Si se escribe un valor no entero, Python termina el programa con un mensaje de error.

### Código del bloque

```
longitud = int(input('Cuantos números deseas capturar?: '))
```

Generación de arreglo  (Pavel, Mauricio)
--------------------------------
### Descripción del bloque

El arreglo *arreglo=[]* se llenará por concatenación con un ciclo *while* en el siguiente bloque.

### Código del bloque

```
arreglo=[]
```

Lectura de números (Pavel, )
----------------------------
### Descripción del bloque

Con base en el valor de la variable *longitud* se concatena cada nuevo valor con la variable *arreglo*
por mediode un ciclo *while*. 
Después se imprimen los números capturados.

### Código del bloque

```
while longitud > 0:
    n = float(input("Número siguiente: "))
    arreglo.append(n)
    longitud -= 1
    
print("Se capturaron los siguientes números", arreglo)
```

Cálculo de la media aritmetica (Pavel, )
------------------------------

### Descripción del bloque

Suma uno a uno los elementos del arreglo *arreglo* con un ciclo *for* y después los divide por el tamaño del arreglo.
Muestra el valor de la media aritmética.

### Código del bloque

```
suma = 0
for i in arreglo:
    suma = suma + i
#    print(suma)

print("la media aritmetica es:", suma/len(arreglo))
```

Programa completo
-----------------

```
#%% Propuesta con arreglos

longitud = int(input('Cuantos números deseas capturar?: '))

arreglo=[]

while longitud > 0:
    n = float(input("Número siguiente: "))
    arreglo.append(n)
    longitud -= 1
    
print("Se capturaron los siguientes números", arreglo)

suma = 0
for i in arreglo:
    suma = suma + i
#    print(suma)

print("la media aritmetica es:", suma/len(arreglo))
```

Propuesta Alternativa (Estrella, Pavel, )
=========================================

Cálculo directo (pero se pierde el detalle de los datos) 

```
numeros = int(input("¿Cuántos números quieres introducir?  ") )
suma = 0
for x in range(numeros):
    suma = suma + float(input("Introduce un número: ") )
    
print ("Se han introducido", numeros, "números que en total han sumado", \\ 

 suma, "y la media es", suma/numeros)
```

Programa: Leer un número impar (Mauricio Jiménez, )
---------------------------------------------------

Leer un número impar por el teclado si el usuario no introduce un numero impar debe repetirse el proceso
hasta que lo introduzca correctamente

```
import math

# El numero cero se define como par

es_impar=0 # Almacena el residuo de la división entre dos

while es_impar==0:
    cadena=input('Capture un numero: ')
    if str.isdigit(cadena):
        numero=int(cadena)
        es_impar=math.fmod(numero,2)
        if es_impar==0:
            print('Debe capturar un numero impar')
    else:
        print('Debe capturar un numero sin decimales')
        es_impar=0
```


Elementos a evaluar  (Alfredo )
===================

La tarea por la que se formó este grupo es que entre todas las personas que estamos en el taller de Python se comenten y se haga una sola reacción a las siguientes temáticas

Capacitación (Pavel,Alfredo )
--------------------

La situación planteada representa un apartado técnico y otro metodológico, ambos con requerimientos especificos.
Las hablidades necesarias para abordar ambos terrenos se relacionan con competencias cognitivas, con competencias
psicosociales y con competencias tecnológicas (más si hablamos desde el terreno de las Habilidades Digitales que
deberían proporcionar los Pilares). En este sentido, la capacitación debe trascender el terreno técnico (Python en este caso)
y abarcar también el terreno psicosocial con ejercicios colectivos, esto es muy sencillo en la teoría: "Enseñemos a 
compartir", pero en la realidad se diluye con un entorno más bien individualista, de competencia y hostilidad generalizada.
Las enseñanzas globales giran en torno a una economía basada en la
productividad, en lo inmediato tanto en las comunicaciones como en las prácticas alimenticias y de convivencia, estamos 
llenos de comida rápida, de comida chatarra y mantenemos lejos del aprendizaje lo que significa producir una hamburguesa
o cualquier otro alimento chatarra, se pierde el aprendizaje, la capacidad (capacitación) de ser autosuficientes, de colectivizar
tareas como el cuidado de la producción y uso de tecnologías, producción de alimentos enajenantes así como de aplicaciones
igualmente enajenantes como las redes sociales, etc. Los entornos formadores comienzan a transitar hacia otras  formas de aprendizaje
como la llamada Educación para la Paz, la resolucion no violenta de conflictos contrastada con la enseñanza de la historia de la
humanidad a travez de las guerras, ¿hay otras historias por aprender? ¿otros ideales que fomentar? PILARES dice que sí
sin embargo aún cuando se plantean mecanismos de educación basados en la inclusión y la cultura del buentrato
(palabra aun no acuñada en el diccionario de la rae), las prácticas apuntan en otra dirección, se sigue implementando
el terrorismo pedagógico, la verticalidad y otras prácticas que desdibujan las buenas intenciones enunciadas. Así la capacitación va
más alla de los lenguajes de programación y debería impregnarse de nuevas (que en la teoría llevan ya  batantes años)
pedagogías, como la ludopedagogía y temas transversales como el lenguaje uncluyente, la diversidad sexual,
la culura del buentrato, perspectiva de género, la inclusión de las PCD, 
LSM, MD, la filosofía del sofware libre (que por cierto es una filosofía de compartir que conlleva implicaciones 
políticas y sociales) y un largo etcétera.
En fin, la capacitación es un terreno fertil, muy fertil, la cuestiónn es cómo apropiárlo, cómo transmtirlo
en el sentido amplio, vinculante hacia las personas y su entorno.

Tenemos  que fomentar la participación, colaboración en el grupo.  


Etapas (Alfredo, )
------------------
Hay que elaborar prácticas dobles para trabajar en equipo 
Con una secuencia fácil de entender y diagramas sencillos. 
Las prácticas no deben durar más de 1:30 una hora y treinta minutos.


Jerarquización (Estrella, Mariana)
--------------
Para este ejercicio primero tuvimos que ponernos de acuerdo en una jerarquización de prioridades para saber qué debía resolverse primero.

Esta técnica nos ayudó a agrupar las ideas. Así se logró clasificar y ordenar toda la información con la que disponíamos y pudimos construir un discurso con las ideas que se reunieron. De igual manera, fué necesario jerarquizar las acciones a realizar en el ejercicio de python para que se ejecutará de manera correcta dicho programa. 

Veamoslo de igual manera dentro de las clases, nosotros como talleristas guiaremos a los jovenes , adultos y niños en busca de conocimiento, hacemos énfasis en jerarquizar el conocimiento proporcionado por edad, estilos de aprendizaje y por interesés. Para esto haremos uso de las herramientas de la pedagogía (para niños) y Andrología (para joevenes y adultos).

Solidaridad  (Apartado Disponible)
----------------------------------

Fomentar la participación grupal para apoyar en comunidad a quien lo requiera.

Participación  (Estrella, Mariana, Alfredo)
-------------------------------------------

No todas las personas aprendemos de la misma manera ni en los mismos tiempos asi que principalmente buscaremos fomentar que las personas sean un poco más empatícas y con ello generar un vínculo más cercano entre las personas usuarias, teniendo en cuenta que no todas asisten la misma cantidad de días y horas podríamos comenzar por ponerles el ejemplo al acercarnos a cada uno para poder ayudarle con sus dudas desde nuestra perspectiva. (Mariana)

Hacer las prácticas participativas y respetuosas para crear confianza. 

Una acción participativa permitirá desarrollar habilidades y capacitar a través del diálogo, la 
autonomía y la conciencia crítica.


División del trabajo  (Pavel, Estrella, Mariana, Alfredo)
------------------------------------------------------------

La división del trabajo implica la comprensión de la problemática para plantear una o varias rutas de solución
ante un planteamiento. Aun cuando el "divide y vencerás" permite conocer las partes que constituyen una solución
en el caso de las ingenierías y otras ramas, dividir el trabajo puede utilizarse como se hace en el modelo de 
producción en serie para aumentar la productividad dada la especializacion en una tarea específica pero se
pierde la globalidad de la situación lo que a su vez impide a las personas apropiarse de todo el proceso.
En el caso de este ejercicó se aprovechó la disección de la problemática para fomentar 
por un lado la documentación y la puesta en marcha de un programa donde diferentes personas peden resolver un bloque para
combinarlo con otros que generarán la solución resolviendo cada parte una tarea específica.

Ésto permitirá lograr objetivos y mejorar la eficiencia, con la especialización y cooperación de las fuerzas laborales en diferentes tareas y roles. 
Como las prácticas serían en pareja esto fomentará la solidaridad y la participación, así como la división del trabajo.
Además de que al hacerlo el resto de personas podrían completar nuestras ideas o enriquecerlas, con suma libertad fomentando el respeto entre los individuos y la interacción de ideas con un mismo fin.(Mariana)


Manipulación de materiales  (Alfredo)
--------------------------------------

Los materiales se entregarán al tutor en caso de ser un educando menor de 15 años se supervisarán
las conexiones de todos antes de poner en servicio los equipos electrónicos para evitar daño al material

Auto didáctica  (Alfredo)
-------------------------

Apoyar y fomentar la imaginación de los educandos 
en la plataforma de tinkercad para que auto exploren y fomenten su creatividad.
La libertad que se da dentro de la programación es bastante amplia pues involucra en parte la perspectiva de la persona frente a un problema planteado, dado que no pensamos de la misma manera, despues de comprender el uso de esta herramienta esta claro que la manera de programación poco a poco se vera más personalizada; nos referimos a que existiran personas que resuelvan las problematicas en algunas lineas de codigo mientras el resto tal vez lo hará en un gran número. Con ello después de entender las funciónes del programa la imaginación se hará latente , y dentro de este mismo la curiosidad cuando las actividades grupales den como resultados una amplía gama de soluciónes a un mismo problema.
Después de ello podremos ver como empujados por la curiosidad poco a poco buscaran diversos métodos de programación y demás que elegiran según lo que para ellos sea más secncillo o amigable.



