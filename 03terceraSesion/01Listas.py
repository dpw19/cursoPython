# -*- coding: utf-8 -*-
"""
Apuntes de la tercera sesion 
Curso de Python en Pilares Benita Galeana

Created on Tue Jun 18 11:08:24 2019

@author: Pavel E Vázquez Mtz
"""

"""
Ejrcicio: Elaborar una actividad para resolverla en pyton

Se te ocurre construir un letrero con leds para la fiesta que vas a
tener antes de salir de vacaciones

En casa tienes un monton de leds, varias resistencias y una fuente
de alimentacion de 12 volts

Cual será el consumo de corriente de un segmento de tu letrero si
conectas en serie cuatro leds con una resistencia de 220 ohms

Recuerda que el votaje de encendido para el del rojo es de 1.2V
y que que V = RI

               R1      D1    D2    D3    D4
    12v o----/\/\/\---->|---->|---->|---->|----o GND
            220 ohm

"""

VD1 = VD2 = VD3 = VD4 = 1.2
VD = VD1 + VD2 + VD3 + VD4
print('VD =', VD, 'V')

# Corriente en la resistencia

I = (12 - VD)/220
print('I =', I*10**3, "mA")



#%% Listas

"""
Una lista es una estructura de datos y un tipo de dato en Python 
con características especiales.
 Nos permite almacenar cualquier tipo de valor como enteros 
cadenas y hasta otras fuciones
"""

mi_lista = ['indice cero', 24, 13.4, True, [1,2]]
print(mi_lista)
print(mi_lista[0])
print(mi_lista[1])
print(mi_lista[2])
print(mi_lista[3])
print(mi_lista[4])

print(mi_lista[-1])

# Modificar un elemento de la lista
mi_lista[0] = False
print(mi_lista)

# Modificar mas de un elemento de una lista
mi_lista[1:4] = [8, 9, 10]
print(mi_lista)

# Colocar nuevo item después del ultimo
mi_lista.append('nuevo elemento')
print(mi_lista)

print(len(mi_lista))

# Agregar nuevo elemento en una posicion específica sin eliminar nada

mi_lista.insert(1, 'insersion')
print(mi_lista)
print(len(mi_lista))

#eliminar elemento específico de una lista

del mi_lista[1]
print(mi_lista)
print(len(mi_lista))

# Eliminar un elemento y guardarlo en otra variable

elemento = mi_lista.pop()
print(mi_lista)
print(elemento)

# Cortar el primer elemento y almacenarlo 
elemento = mi_lista.pop(0)
print(mi_lista)
print(elemento)

#%% Ordenar listas
# Ordenar una lista

letras = ['B', 'C', 'A', 'b', 'c', 'a']
print(letras)
letras.sort()
print(letras)

letras.reverse()
print(letras)

#%% Ordenar listas
# Ordenar una lista

numeros = [1, 5, 7, 100, 88, 2]
print(numeros)
numeros.sort()
print(numeros)

numeros.reverse()
print(numeros)

#%% Ordenar listas
# Ordenar una lista

booleanos = [True, False, False, True, True, False]
print(booleanos)
booleanos.sort()
print(booleanos)

booleanos.reverse()
print(booleanos)

#%% Generar una lista de números

numeros = list(range(1,6))
print(numeros)

pares = list(range(2, 11, 2))


# Máximos y mínimos

print(max(numeros))
print(min(numeros))
print(sum(numeros))

# Rebanadas

print(numeros)
print(numeros[1:4])
print(numeros[:4])
print(numeros[1:])

# Copiar lista

mis_comidas = ['Tortas', 'Taquitos', 'Tamales']
comidas_amigo = mis_comidas[:]
print(mis_comidas)
print(comidas_amigo)

# Operaciones con listas

print('Concatenar listas')
print(['a', 'b', 'c'] + ['d', 'e', 'f'])

# repeticion
print(['a', 'b', 'c'] * 3)

#%% Tuplas
"""
Conjunto ordenado e inmutable de elementos del mismo o diferente tipo
Se puede leer
"""

mi_lista=['indice cero', 24, 13.4, True, [1,2]]
mi_tupla=('indice cero', 24, 13.4, True, [1,2])



#%% 
