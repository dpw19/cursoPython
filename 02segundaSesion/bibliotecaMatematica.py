# -*- coding: utf-8 -*-
"""
Apuntes de la segunda sesion
Curso de Python en Benita Galeana

Created on Tue Jun 11 11:14:24 2019

@author: Pavel E. Vazquez Mtz
"""

import math
import arcpy

print("1.001 con ceil da: ", math.ceil(1.001))

print("1.001 con floor da: ", math.floor(1.001))

help(math)

#%% Bloque
help(math.ceil)

#%%
#Absoluto de 5
print(abs(-5))
# Absoluto conbiblioteca math.fabs
print(math.fabs(-5))
# de que tipo es???
print(type(math.fabs(-5)))  
# factorial de un número
print(math.factorial(4))
# truncar un número
print(math.trunc(1.001))

#%% Funciones trigonométricas

print(math.pi)
print(math.sin(math.pi/2))
print(math.cos(math.pi/2))
print(math.tan(math.pi/2))
print(math.hypot(12,5))
print(math.atan(math.pi/2))

#%% Importando funciones para acortar sintaxis

from math import pi
"""Funciones senoidales"""

from math import sin
from math import cos
from math import tan
from math import atan
from math import hypot

"""funciones hiperbólicas"""
from math import sinh
from math import cosh
from math import tanh
from math import asinh
from math import acosh
from math import atanh

"""Funciones Logarítmicas"""
from math import exp
from math import e
from math import log
from math import pow
from math import sqrt

print("************", "Senoidales", "************")
print(pi)

print(sin(pi/2))
print(cos(pi/2))
print(tan(pi/2))
print(hypot(12,5))
print(atan(pi/2))

print(math.radians(180))

print("************", "Hiperbólicas", "************")
print(sinh(pi/2))
print(cosh(pi/2))
print(tanh(pi/2))
print(asinh(pi/2))
print(acosh(pi/2))
print(atanh(0))

print("************", "Exponentes", "************")
print(exp(5))
print(e**5)
print(log(148.4131))
print(pow(144, 0.5))
print(sqrt(144))


#%% Ejercicio a resolver Suma de vectores

import math
from math import sin
from math import cos
from math import atan
from math import sqrt
from math import pow
from math import radians
from math import pi
from math import degrees



# Magnitudes de los vectores
f1 = 35
f2 = 30

# Angulos de los vectores estan dados en grados
a = 50
b= 25

# Grados a radianes

a_rad = radians(a)
b_rad = radians(b)

## Descomposicion de vecores en sus componentes en x y en y

f1x = 35 * cos(a_rad)
f1y = 35 * sin(a_rad)

f2x = 30 * cos(b_rad)
f2y = 30 * sin(b_rad)

## Suma de fuerzas en x
frx = f1x + f2x

## Suma de fuerzas en y
fry = f1y + f2y

# Magnitud de la fuerza resultante

fr = sqrt(pow(frx,2) + pow(fry,2))
print(fr)

c = atan(fry/frx)

#Convertimos c a grados e imprimimos

print(degrees(c))


#%% Operadores Logicos y aritméticos

a = 1
b = 2

print("a > b", a > b)
print(a < b)
print(a == b)
print(a >= b)
print(a <= b)
print(a != b)

print('verde' != 'azul')

print('verde' != 'AZUL')

print(a > 0 and b > 0)
print(a > 0 or a > 10)

print(not a > 0)

print(1>10 and 'uno' == 'uno')
print(100>10 and 'uno' == 'uno')
print(100>10 or 'uno' == 'dos')
print(1 < 10 or 'uno' == 'dos')
print(not 100>10)

#%% Ejemplo Estructura codigos postales

codigos_postales=[
        41510,
        30709,
        21748,
        84175]

print(41510 in codigos_postales)
print(41510 not in codigos_postales)

#%% Ejercicio:
# Realizar un programa que lea 2 numeros y determine los siguientes aspectos
# Si los numeros son iguales
# Si los numeros son diferentes
# Si el primero es mayor que el segundo
# Si el segundo es mayor que el primero


n1 = float(input("introduce el primer numero: "))
n2 = float(input("introduce el segundo numero: "))

print('');
print("¿n1 es igual a n2?:", n1 == n2)
print("¿n1 es diferente a n2?:", n1 != n2)
print("¿n1 es mayor a n2?:", n1 > n2)
print("¿n1 es igual a n2?:", n1 < n2)




#%% Ejercicio 2
# Utilizando operadores lógicos determina si una cadena de texto
#introducida por el usuario  tiene una longitud mayor o igual que 3
# y a su vex es menor que 10

cadena =  input('Escribe un texto: ')

#cadena = 'este es un texto'

print('La cadena es:', cadena)
l = len(cadena)
print('Su longitud es', l)

print('Es mayor igual que 3 y a su vez es menor que 10?: ', l >= 3  and l < 10);




