Pilares_Python_2019
===================

** Creación: ** 25/06/2019

** Propósito: ** Actividad colectiva para solucionar el problema de programación planteado
en la cuarta sesión del curso de Python en el PILARES Benita Galeana

** Fuente: ** https://pad.riseup.net/p/Pilares_Python_2019

Problema a resolver
===================

Resolver con Python el código corporal siguiente:

Nota: Alfredo  solo para comentar que la palmada se cambie por aplauso

Posición 0 Erick:
Palmada,
palmada, chasquido, chasquido,
Palmada,
Palmada,
Chasquido,
Chaquido,

Posición 1 Pavel:
Palmada en muslo,
Palmada en muslo,
Palmada.

Posición 2 Marco:
Palmada en hombros
Palmada en hombros
Palmada

Posición 3 Alfredo:
Palmada

Posición 4 Matiana (Miau)
Palmada
Palmada
Palmada
Palmada sensual en el muslo
Palmada sensual en el muslo
Palmada sensual en el muslo
Chasquido
Chasquido
Chasquido
Toque en la nariz

Posicion 5 (Mauricio Quintero)
Palmada en el muslo
Tocar hombros con brazos cruzados
Palmada de

Posicion 6 (Mauricio Jiménez)
Tocar la oreja derecha con la mano izquierda

Posición 7 Estrella:
Brazos cruzados.

Metodología
===========

Anáisis Previo ¿Qué vamos a hacer?)
--------------
Hacer un programa en Phyton que despliegue los códigos de los usuarios anteriores y pida al usuario sus  codigos  nuevos
Funcionamiento:  Al  primer  usuario  posicion 0 no se le desplegrá ningun código anterior solo se le pedirán sus nuevos
códigos  uno  por  uno,  hasta  que  de  el código LISTO (con lo cual indicará que ya terminó) y el programa cambiara de
usuario, desplegando los códigos que tiene guardados antes de pedir los codigos al nuevo usuario.
El último usuario no dará el codigo LISTO en su lugar dará  el  codigo  SALIR  (significa que este usuario es el último)
y el programa desplegara todos los codigos almacenados y terminará su ejecución

Espero les guste esta propuesta de funcionamiento atte Mauricio Jiménez

Propuesta 2
Programa que despliegue el código corporal generado por el grupo de trabajo. 

### Qué datos se necesitan
De acuerdo al funcionamiento anterior tenemos dos ciclios anidados El ciclo externo encargado de pedir los usuarios
y el ciclo interno encargado de pedir los codigos de cada usuario y podria funcionar con una lista de listas 
codigos[['codigo1 usuario1','codigo2 usuario1', ... ,'codigoN usuario1'],
['codigo1 usuario2','codigo2 usuario2', ... ,'codigoN usuario2'], ... ,
['codigo1 usuarioN','codigo2 usuarioN', ... ,'codigoN usuarioN']]
las constantes 'LISTO' y 'SALIR' que son las cadenas que el programa reconocerá para cambio de usuario y finalizar
la captura

Propuesta 2
El código corporal está descrito en una lista de listas, cada posición es un segmento del código



### De donde provienenlos datos
De los diferentes usuarios que ingresan sus codigos a la computadora

Propuesta 2:
Del segmento de código que cada persona agregó, están listados en el grupo de whatsapp.

### Cómo se obtienen los datos
Mediente el tecado con la instrucción input

Propuesta 2:
Están capturados como un dato tipo lista.

### Cómo interactuan entre sí los datos
No permitir que el usuario ingrese un código nulo (cadena en blanco).
Códigos almacenandose y recuperandose de la memoria constantemente
con cada captura de código checar si es LISTO y con cada cambio de usuario checar si es SALIR
para realizar las acciones correspondientes

Propuesta 2:
    
Se recorre la lista y se despliega cada valor de la lista de listas con un breve retardo.

### Cual será el resultado
El crecimiento progresivo de la lista de códigos almacenados con cada cambio de usuario y al final la lista
de todos los codigos capturados por la totalidad de los usuarios.
Espero que con este planteamiento ya se tenga mas claro como diseñar el programa que cubra tal fin atte Mauricio Jimenez
(me conecto mañana en la tarde para ver si alguien ya realizó algun avance con los siguientes puntos).

propuesta 2:
  Aparecerán una a una las partes del código corporal.

Diseño del algoritmo (como lo vamos a hacer?
---------------------

### Pseudocódigo

Propuesta 2:

```    
codigoCorporal = [
    [lista0],
    [lista1],
    ...
    [lista6],
    [lista7]
]

para level1 en codigoCorporal:
    para level2 en level1:
        imprime(level2)
        retardo
```


### Diagrama de flujo


Configuración del entorno de desarrollo y codificación
------------------------------------------------------

Se corre sobre GNU/linux

Python 3

Se requiere el módulo **time.sleep**

Complilación y pruebas
----------------------
Detectar errores en tiempo de compilacion, ejecución y lógicos

Propuesta dos:
    
```
import time

codigoCorporal = [
# Posicion 0 Erick
["Palmada",
"palmada",
"chasquido",
"chasquido",
"Palmada",
"Palmada",
"Chasquido",
"Chaquido"],
# Posicion 1 Pavel
["Palmada en muslo",
"Palmada en muslo",
"Palmada"],
# Posición 2 Marco
["Palmada en hombros",
"Palmada en hombros",
"Palmada"],
# Posición 3 Alfredo:
["Palmada"],
# Posición 4 Matiana (Miau)
["Palmada",
"Palmada",
"Palmada",
"Palmada sensual en el muslo",
"Palmada sensual en el muslo",
"Palmada sensual en el muslo",
"Chasquido",
"Chasquido",
"Chasquido",
"Toque en la nariz"],
# Posicion 5 (Mauricio Quintero)
["Palmada en el muslo",
"Tocar hombros con brazos cruzados",
"Palmada de"],
# Posicion 6 (Mauricio Jiménez)
["Tocar la oreja derecha con la mano izquierda"],
# Posición 7 Estrella
["Brazos cruzados"]
]

for level1 in codigoCorporal:
    print("")
    for level2 in level1:
        print(level2)
        time.sleep(0.8)


```

Documentación y mantenimiento
-----------------------------
