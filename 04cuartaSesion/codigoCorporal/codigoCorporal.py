import time

codigoCorporal = [
# Posicion 0 Erick
["Palmada",
"palmada",
"chasquido",
"chasquido",
"Palmada",
"Palmada",
"Chasquido",
"Chaquido"],
# Posicion 1 Pavel
["Palmada en muslo",
"Palmada en muslo",
"Palmada"],
# Posición 2 Marco
["Palmada en hombros",
"Palmada en hombros",
"Palmada"],
# Posición 3 Alfredo:
["Palmada"],
# Posición 4 Matiana (Miau)
["Palmada",
"Palmada",
"Palmada",
"Palmada sensual en el muslo",
"Palmada sensual en el muslo",
"Palmada sensual en el muslo",
"Chasquido",
"Chasquido",
"Chasquido",
"Toque en la nariz"],
# Posicion 5 (Mauricio Quintero)
["Palmada en el muslo",
"Tocar hombros con brazos cruzados",
"Palmada de"],
# Posicion 6 (Mauricio Jiménez)
["Tocar la oreja derecha con la mano izquierda"],
# Posición 7 Estrella
["Brazos cruzados"]
]

for level1 in codigoCorporal:
    print("")
    for level2 in level1:
        print(level2)
        time.sleep(0.8)
