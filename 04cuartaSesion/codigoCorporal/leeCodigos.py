# -*- coding: utf-8 -*-
"""
Created on Sun Jun 30 13:06:38 2019

@author:
    Pavel E. Vázquez Mtz.
"""    

codigo_individual=[]
codigo_general=[]

n=int(input('Cantidad de Participantes: '))
nuevo_codigo = 'y';

for i in range(n):
    print('participante', i+1, ':')
    while nuevo_codigo == 'y':
        codigo_individual.append(input('Codigo:'))
        nuevo_codigo = input('¿uno más? (y/n): ')
#        print(nuevo_codigo)
        while nuevo_codigo != 'y' and nuevo_codigo != 'n':
            nuevo_codigo = input('opcion incorrecta, ¿uno más? (y/n): ')
    codigo_general.append(codigo_individual)
    codigo_individual = []
    nuevo_codigo='y'

print(codigo_general)