#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 20:15:59 2019

@author: Pavel E. Vazqez Mtz.
"""

# Programa para graficar un triángulo dados sus puntos
# basado en https://www.programoergosum.com/cursos-online/raspberry-pi/245-turtle-graphics-en-python-con-raspberry-pi/figuras-geometricas


import turtle
import time
from math import atan
from math import degrees

x1=0
y1=0

x2=5
y2=0

x3=3
y3=5

m1 = degrees(atan ((y2 - y1)/(x2 - x1)))

m2 = degrees(atan ((y2 - y3)/(x2 - x3)))

m3 = degrees(atan ((y1 - y3)/(x1 - x3)))

d1 =  ((y2 - y1)**2 + (x2 - x1)**2)**(0.5)
d2 =  ((y2 - y3)**2 + (x2 - x3)**2)**(0.5)
d3 =  ((y3 - y1)**2 + (x3 - x1)**2)**(0.5)


print(m1, m2, m3)
print(d1, d2, d3)


window = turtle.Screen()
flecha = turtle.Turtle()

time.sleep(3)

flecha.left(m1)
flecha.forward(d1 * 10)

flecha.left(180 - m2)
flecha.forward(d2 * 10)

flecha.left(180 - m3)
flecha.forward(d3 * 10)

time.sleep(3)

