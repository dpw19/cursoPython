#%% Calcula el área de un tríangulo formado por tres puntos.

from math import sqrt
from math import acos
from math import sin
from math import sqrt
from math import pow
from math import degrees

x1 = float(input("X1: "))
y1 = float(input("y1: "))

x2 = float(input("X2: "))
y2 = float(input("y2: "))

x3 = float(input("X3: "))
y3 = float(input("y3: "))

#confirma coordenadas
print("p1(", x1, ",", y1, ")")
print("p2(", x2, ",", y2, ")")
print("p3(", x3, ",", y3, ")")

#calculo de los segmentos a, b y c
a = sqrt((x2- x1)**2 + (y2 - y1)**2)
b = sqrt((x3- x1)**2 + (y3 - y1)**2)
c = sqrt((x3- x2)**2 + (y3 - y2)**2)

# print("a=", a)
# print("b=", b)
# print("c=", c)

alpha = acos(-(c**2 - a**2 - b**2)/(2*a*b))
h = a*sin(alpha)
area = b*h/2

print("area = " , area, "u^2")

if alpha==0:
    print ("Los puntos no perenecen a un triángulo")
