# Curso de Python

En este repositiorio se encentran los apuntes personales
(archivos) generados durante el curso de Python en el 
PILARES Benita Galeana.

Cada carpeta contiene los archivos generados y tareas
para esa sesión.

A grandes rasgos los temas son los siguientes:

## Primera sesión:
* Print
* Concatenación
* Tipos de datos
* Operaciones básicas

## Segunda sesión
* Biblioteca matemática
* Importar funciones
* Operadores lógicos y aritméticos
* Estructuras

## Tercera sesión
* Listas
* Estructuras de Control if, while y for
* Tarea colectiva

## Cuarta sesión
* Tarea individual: Área del triángulo dadas las coordenadas de sus puntos.
* Tarea colectiva : Código corporal

## Quinta sesión
* Funciones
* Módulos

## Sexta sesion
* Diccionarios
* Gráfica de datos

## Septima Sesión
* Gráfica de datos
* Arhivos

## Octava sesión
* Interface Gráfica de Usuario GUI

## Calculadora
* Calculadora gráfica 
