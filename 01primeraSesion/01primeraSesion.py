# -*- coding: utf-8 -*-
"""
Apuntes de la Primera seisión 
Curso Python en Pilares Benita Galeana

Author : Pavel E. Vázquez Mtz.
"""

print ("Hola mundo")
a=5+5
print(a)

# Concatenacion de palabras

print("hola", "como", "estas")

print('hola', 'como', 'estas')

print('hola' + 'como' + 'estas')

''' Este tambien es un comentario '''

#%% Bloque suma de flotantes

a = 10.5
b = 12.3
print (a-b)
 

#%% Variables y objetos
var_entera = 1
print(var_entera)

var_real = 1.6
print(var_real)

var_compleja = 1 - 4.5j
print(var_compleja)

var_text = 'usando comillas simples'
print(var_text)

var_text2 = "usando comillas dobles"
print(var_text2)

var_boolean = True
print(var_boolean)

print(type(var_boolean))

print(type("8"))
print(type('8'))

print(len (var_text))

#%%
suma = 2+2+4+8
resta = 10 - 5
multiplica = 10 * 100
dividir = 27 / 4
residuo = 27 % 4
exponente= 3 ** 3

print("El resultado de la suma es", suma)
print("El resultado de la resta es", resta)
print(multiplica)
print(dividir)
print(residuo)
print(exponente)
 
#%% Calcular la superficie de un triángulo

base = 3
altura = 5

area = base * altura / 2
print("El area es:" , area)

#%% Calcula el area de una circunferencia
r = 3
pi = 3.1416
area1 = pi * r ** 2
area2 = pi * r * r

print("Con r**2 da:",area1)
print("Con r*r da:", area2) 

#%% La chicharronera
#ax^2 + bx +c = 0

a = 2
b = 5
c = 3

res1 = (-b + (b**2 - 4*a*c)**(1/2))/(2*a)

res2 = (-b - (b**2 - 4*a*c)**(1/2))/(2*a)

print ("res1:", res1)
print ("res2:", res2)
