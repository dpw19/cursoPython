# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 11:10:49 2019

@author: Pavel E. Vazquez Mtz
Apuntes sobre el curso de Python en Pilares Benita Galeana
"""

from pylab import *

x = arange(10.)
plot(x, '-')
show()

#%%

from pylab import *

x = arange(10.)
x2 = x**2
x3 = x**3
plot(x,x,'b.',x,x2,'rd',x,x3,'k^')
#xlim(0,5)
#ylim(0,150)
show()

#%% Linspace

'''
Genera un arreglo 
help(linspace)
'''

help(linspace)
linspace(0, 10, 20)

#%% Instanciando las graficas

from pylab import*

x = arange(10.)
x2 = x**2
x3 = x**3

p1, p2, p3 = plot(x,x,'k-',x,x2,'+-',x,x3,'g.')

# Muestra la informacion

p1.set_marker('*') # Cambia el símblo de la grafica 1
p3.set_color('y') # Cambia el color de la grafica 3

draw()

#%% Texto en la gráfica

from pylab import*

x = arange(0,5,0.05)

plot (x, log10(x)*sin(x**2))

xlabel('Eje x')
ylabel('Eje y')
title('Mi gráfica')
grid()

text(1,-0.4, 'Esta es una nota')


#%% 

from pylab import*
from numpy import*

t = arange(0.1,20,0.1)

y1 = sin(t)/t
y2 = sin(t)*exp(-t)

p1,p2 = plot(t,y1,t,y2)

text1=text(2,0.6,r'$\frac{\sin(x)}{x}$', fontsize=20)

text2=text(13,0.2,r'$\sin(x) \cdot e^{(-x)}$', fontsize=14)

grid()

title('Representación de dos funciones')
xlabel('Tiempo [s]')
ylabel('Amplitud [cm]')

show()

#%% Reprsentacion de funciones

from pylab import*
from numpy import*

def f1(x):
    y = sin(x)
    return y

def f2(x):
    y=sin(x) + sin (5.0*x)
    return y

def f3(x):
    y = sin(x) * exp(-x/10.)
    return y

#print(f1(9))
#print(f2(9))
#print(f3(9))
    
x = linspace(0,10*pi,800)

pi,p2,p3=plot(x,f1(x),x,f2(x),x,f3(x))

legend(('$\sin(x)$', '$\sin(x) + \sin(5 \cdot x)$', '$\sin(x) \cdot e^{-x/10}$'),prop={'size':10},loc='upper right')

xlabel('Amplitud [s]')
ylabel('Amplitud [cm]')
title('Representación de 3 funciones')


#%% 

from pylab import*

x=arange(0,2*pi,0.05)

figure(1)
p1 = plot(sin(x))
title('Funcion senoidal')

figure(2)
p1 = plot(cos(x))
title('Funcion cosenoidal')

#%% Varios gráficos en una ventana

from pylab import*
from numpy import*

def f1(x):
    y = sin(x)
    return y

def f2(x):
    y=sin(x) + sin (5.0*x)
    return y

def f3(x):
    y = sin(x) * exp(-x/10.)
    return y
    
x = linspace(0,10*pi,100)

subplot(221)

p1 = plot(x,f1(x),'r-')
title('Funcion 1')
xlabel('Eje x')
ylabel('Eje y')
grid()

subplot(222)

p2 = plot(x,f2(x),'g*-')
title('Funcion 2')
xlabel('Eje x')
ylabel('Eje y')
grid()

subplot(223)

p2 = plot(x,f3(x),'bd-')
title('Funcion 3')
xlabel('Eje x')
ylabel('Eje y')
grid()

savefig('mifigura.eps')
savefig('mifigura.pdf')
savefig('mifigura.png',dpi=300)

