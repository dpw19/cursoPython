# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 13:25:42 2019

@author: Pavel E. Vazquez Mtz. 
Trabajo con archivos
"""

# Crear un archivo y escribir en el

f=open('datos.txt', 'w')
f.write('Pavel E. Vázquez Martínez')
f.close()

#%% Leer un archivo de texto y mostrar su contenido

f = open('datos.txt', 'r')
mensaje = f.read()
print(mensaje)
f.close()

#%% Preguntar si es posible desplazarse dentro de archivo

f=open('datos.txt', 'r')
print(f.seekable())
f.close()

#%% De qué tipo es el archivo

f=open('datos.txt', 'r')
print(type(f))
f.close()

#%% Ejemplo 5 
'''
1. Se nos pedirá que se cree un archivo de texto nuevo con el nombre prueba.txt
2. Al objeto se le asignará el nomvre de archivo
3. Se escribiran tres líneas
4. Se desplegará la posicion del puntero sobre el archivo
5. se cerrará el archivo
'''

archivo = open('prueba.txt', 'w')
archivo.write('Hola.\nBienvenido al curos de Python.\nEsperamos que sea una agradable experiencia')
print(archivo.tell())
archivo.close();

#%% Ejemplo 6

'''
1. Abrir el archivo prueba.txt como solo lectura
2. Asignar al objeto el nombre archivo
3. Leer la primera lina de texto
4. Desplegar la primera linea
5. cerrar el archivo
'''

archivo = open('prueba.txt', 'r')
print(archivo.readline())
archivo.close

#%% Ejemplo 7 

'''
1. Abrir el archivo prueba.txt como solo lectura
2. Asignar al objeto el nombre archivo
3. Localizar el puntero en la posicion 12 del archivo
4. Leer todas las lineas de texto a partir de esa posición
5. Desplegar cada línea
6. Cerrar archivo
'''

archivo = open('prueba.txt', 'r')
archivo.seek(12)
for linea in archivo.readlines():
    print(linea)
archivo.close()

#%% Ejemplo 8 

'''
1. Abrir el archivo prueba.txt como escritura no destructiva, posicionar el
puntero al final del archivo.
2. Asignar al objeto el nomvre archivo
3. Desplegar la posicion del puntero
4. Añarir dos líneas de texto
5. Desplegar la nueva posición del puntero
6. Cerrar el archivo 
'''

archivo = open('prueba.txt','a')
print(archivo.tell())
archivo.write('\nNueva Línea.\nAquí.\nY allá.')
print(archivo.tell())
archivo.close()

#%% Ejemplo 9

'''
1. Abrir el archivo prueba.txt como lectura escrtitura
2. Asignar al objeto el nombre archivo
3. Sobreescribir el teto inicial
4. Posicionar el puntero al inicio del archivo
5. Desplegar todo el texto a partir de la posicion inicial
6. cerrar el archivo 
'''

archivo = open('prueba.txt', 'r+')
archivo.write('HOLA')
archivo.seek(0)
print(archivo.read())
archivo.close()


#%% Imprime la linea de un archivo con su número

archivo = open('letra.txt', 'r')
i=1
for linea in archivo:
    linea=linea.rstrip()
    print(i, linea)
    i += 1
archivo.close





