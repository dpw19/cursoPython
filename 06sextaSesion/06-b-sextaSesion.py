# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 14:36:12 2019

@author: Pavel E. Vázquez Mtz.
Apuntes personales del curso de Pyton en el pilares Benita Galeana
"""

from pylab import *

x = arange(10.)
plot(x, 'x-')
show()

#%%

from pylab import *

x = arange(10.)
x2 = x**2
x3 = x**3
plot(x,x,'b.',x,x2,'rd',x,x3,'g^')
xlim(0,5)
ylim(0,500)
show()
