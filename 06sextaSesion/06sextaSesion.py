# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 11:09:59 2019

@author: Pavel E. Vazquez Mtz

Apuntes personales del curso Python en Piares Benita Galeana

Sexta sesión Diccionarios

"""



mi_diccionario={'clave_1': 'Valor', 'clave_2' : 2, 'clave_3' : 3.33}

print(mi_diccionario['clave_2'])
print(mi_diccionario)

# Sobreescribir

mi_diccionario['clave_3']='nuevo_valor'
print(mi_diccionario)

# Eliminar una clave
del(mi_diccionario['clave_1'])
print(mi_diccionario)

# Lista dentro del diccionario

mi_diccionario2={'clave_1': 1, 'clave_2': 2, 'clave_3': 3.33, (1,2) : [5,6,7]}

print(mi_diccionario2)
print(mi_diccionario2[1,2])

print(mi_diccionario2[1,2][0])

#%% Diccionario de Libros

libros={'Salambo' : 'Gustave Flaubert', 'Lucien Leuwen' : 'Stendhal', 'La momia' : 'Theophili Gauthier'}

print(libros)

print(libros['Salambo'])

#%% Grupo Música

Grupo_musica = {'Nombre' : 'Funeral',
                'Genero' : 'Funeral Doom',
                'Pais' : 'Noruega',
                'Numero de elementos':'5',
                'Disco favorito':'In Fields of pertilence Grief'
                }

for key, value in Grupo_musica.items():
    print('Key:' +key)
    print('Value:' +value)
    print


#%% Grupo Música

Grupo_musica = {'Nombre' : 'Funeral',
                'Genero' : 'Funeral Doom',
                'Pais' : 'Noruega',
                'Numero de elementos':'5',
                'Disco favorito':'In Fields of pertilence Grief'
                }

for key, value in Grupo_musica.items():
    print(key, ': ', value)
    print

#%% Literatura Inglesa
    
literaturaInglesa={
        'Historia de dos ciudades':'Charles Dickens',
        'El enano negro':'Walter Scott',
        'Cumbres borrascosas':'Emili Bronte',
        'La isla del tesoro':'Robert Louis Stevenson',
        'Frankestein':'Marie Shelley',
        'El matrimonio del cielo y el infierno':'William Blake',
        'El hombre que fue jueves':'Og. K. Chesterton',
        }

# Desplegar los keys

for libro in literaturaInglesa.keys():
    print(libro)
    
print()

for author in literaturaInglesa.values():
    print(author)

for libro, author in literaturaInglesa.items():
    print('Libro: ' + libro)
    print('Autor: ' + author)
    print()
    

#%% Evitar valores repetidos
    
literaturaInglesa={
        'Historia de dos ciudades':'Charles Dickens',
        'El enano negro':'Walter Scott',
        'Cumbres borrascosas':'Emili Bronte',
        'La isla del tesoro':'Robert Louis Stevenson',
        'Frankestein':'Marie Shelley',
        'Frankestein':'Marie ',
        'El matrimonio del cielo y el infierno':'William Blake',
        'El hombre que fue jueves':'Og. K. Chesterton',
        'que fue jueves':'Og. K. Chesterton',
        }

# Evita autores repetidos
for author in literaturaInglesa.values():
    print(author)
    
    
#Evitar Libros repetidos 
for libro in literaturaInglesa.keys():
    print(libro)


for libro, author in literaturaInglesa.items():
    print('Libro: ' + libro)
    print('Autor: ' + author)
    print()

# Métodos
    
print(len(literaturaInglesa.values()))
print(max(literaturaInglesa.values()))
print(min(literaturaInglesa.values()))
    

#%% Ejercicio
'''
Programa que crea in diccionario con datos personales para un determinado
número de personas

clave:codigo
valores asociados: nombre y edad
'''

# Análisis del problema
'''
Lee desde el teclado el numero de registros a capturar y 
la clave asociada a un nombre y edad
'''

# Tipo de datos
'''
n es el nimero de datos a capturar
diccionario llamado registros con las siguientes
llaves

registro{
        codigo: [nombre, edad]}
}
'''

# Pseudocódigo
'''
n = lee nunmero de registros a capturar

registro={}

mientras n<0
    captura registro[codigo: [edad nombre]]

muestra registro
'''


n = int(input('Cuantos registros deseas capturar: '))

print('Se van a capturar', n, 'registros')

registro={}

while n > 0:
    print('Registro', n, ':')
    codigo = int(input('Número de código: '))
    nombre = input('nombre: ')
    edad = int(input('Edad: '))
    
    registro[codigo] = [nombre , edad]
    n -= 1

print
print(registro)

#%%
## Seginda parte generar un programa para buscar una clave en el diccionario 
## creado

'''
Analisis del programa

Se tiene un registro con los registros previamente capturados
hay que solicitar numero de registro y mostrar su contenido

Datos del progrma

Registro con capturas: ya esta dado
numero de registro: se captura desde el teclado
Paeudocodigo

n = Qué registro deseas revisar

imprime registro[n][0]
 imprime registro[n][1]
 
'''
222
n = int(input('Qué registro deseas revisar: '))
print('Nombre: ', registro[n][0])
print('Edad: ', registro[n][1])


