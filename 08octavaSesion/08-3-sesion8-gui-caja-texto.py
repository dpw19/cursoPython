# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:36:07 2019

@author: Pavel E. Vázquez Mtz.
"""

''' Introduccion al las interfaces graficas '''


from tkinter import*

gui = Tk()
gui.title('Usando cajas de texto')
gui.geometry('500x500') # ancho y alto
gui.configure(bg = 'gray')

etiqueta21 = Label(gui, text= 'Nombre: ')
etiqueta21.grid(row = 1, column = 1)

variable_string = StringVar()
caja_texto = Entry(gui, textvariable = variable_string)
caja_texto.grid(row=1, column=2) 


gui.mainloop()

