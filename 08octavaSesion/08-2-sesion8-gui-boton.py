# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:36:07 2019

@author: Pavel E. Vázquez Mtz.
"""

''' Introduccion al las interfaces graficas '''


from tkinter import*

gui = Tk()
gui.title('Colocar botones')
gui.geometry('500x500') # ancho y alto
gui.configure(bg = 'gray')

button_11 = Button(gui,text='Boton 1,1', bg="red")
button_11.grid(row=1, column=1)

button_22 = Button(gui,text='Boton 2,2', bg="yellow", relief=FLAT)
button_22.grid(row=2, column=2)

button_33 = Button(gui,text='Boton 3,3', bg="green", relief=SUNKEN)
button_33.grid(row=3, column=3)

button_44 = Button(gui,text='Boton 4,4', bg="blue", relief=RIDGE)
button_44.grid(row=4, column=4)

button_55 = Button(gui,text='Boton 5,5', bg="yellow", relief=SOLID)
button_55.grid(row=5, column=5)

gui.mainloop()

