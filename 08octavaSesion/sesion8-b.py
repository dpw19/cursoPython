# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:36:07 2019

@author: Pavel E. Vázquez Mtz.
"""

''' Introduccion al las interfaces graficas '''


from tkinter import*

gui = Tk()
gui.title('Interface de usuario')
gui.geometry('500x500') # ancho y alto
gui.configure(bg = 'blue')

label_11 = Label(gui,text='Etiqueta numero 1,1')
label_11.grid(row=1, column=1)

label_22 = Label(gui,text='Etiqueta numero 2,2')
label_22.grid(row=2, column=2)

gui.mainloop()

