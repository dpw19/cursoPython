# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:36:07 2019

@author: Pavel E. Vázquez Mtz.
"""

''' Introduccion al las interfaces graficas '''


from tkinter import*

gui = Tk()
gui.title('Registro de clientes')
gui.geometry('500x500') # ancho y alto
gui.configure(bg = 'gray')

etiqueta11 = Label(gui, text= 'Registro de clientes ')
etiqueta11.grid(row = 1, column = 1)

etiqueta21 = Label(gui, text= 'Nombre: ')
etiqueta21.grid(row = 2, column = 1)

etiqueta31 = Label(gui, text= 'Apellidos: ')
etiqueta31.grid(row = 3, column = 1)

etiqueta21 = Label(gui, text= 'RFC: ')
etiqueta21.grid(row = 4, column = 1)

variable_string = StringVar()
caja_texto = Entry(gui, textvariable = variable_string)
caja_texto.grid(row=2, column=2) 

variable_string = StringVar()
caja_texto = Entry(gui, textvariable = variable_string)
caja_texto.grid(row=3, column=2) 

variable_string = StringVar()
caja_texto = Entry(gui, textvariable = variable_string)
caja_texto.grid(row=4, column=2) 

button_51 = Button(gui,text='Nuevo', bg ='green')
button_51.grid(row=5, column=1)

button_52 = Button(gui,text='Borrar')
button_52.grid(row=5, column=2)

button_53 = Button(gui,text='Eliminar', bg='red')
button_53.grid(row=5, column=3)


gui.mainloop()

