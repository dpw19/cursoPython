# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:36:07 2019

@author: Pavel E. Vázquez Mtz.
"""

''' Introduccion al las interfaces graficas '''


from tkinter import*

def click_1():
    try:
        valor_1 = int(entrada_texto_1.get())
        valor_1 = valor_1 * 2
        etiqueta_1.config(text=valor_1)
        print(valor_1)
    except ValueError:
        etiqueta_1.config(text='Introduce un numero')
        print('Entroduce un número')

def click_2():
    try:
        valor_1 = str(entrada_texto_1.get())
        valor_1 = valor_1 * 2
        etiqueta_1.config(text=valor_1)
        print(valor_1)
    except ValueError:
        etiqueta_1.config(text='Introduce un numero')
        print('Entroduce un número')

gui = Tk()
gui.title('Registro de clientes')
gui.geometry('500x500') # ancho y alto
gui.configure(bg = 'gray')

etiqueta_1 = Label(gui, text= 'Introduce un numero que será multiplicado por dos')
etiqueta_1.grid(row = 1, column = 2)


boton_1 = Button(gui,text='Opera', width=10,bg='red', command=click_1)
boton_1.grid(row=2, column=1)

valor_1 = ''

entrada_texto_1 = Entry(gui, width= 10, textvariable = valor_1)
entrada_texto_1.grid(row=2, column=2)

etiqueta_1=Label(gui, text='Resultado') 
etiqueta_1.grid(row=2, column=3)


etiqueta_2 = Label(gui, text= 'Introduce una cadena que será multiplicado por dos')
etiqueta_1.grid(row = 5, column = 2)


boton_1 = Button(gui,text='Opera', width=10,bg='red', command=click_2)
boton_1.grid(row=5, column=1)

valor_2 = ''

entrada_texto_1 = Entry(gui, width= 10, textvariable = valor_2)
entrada_texto_1.grid(row=5, column=2)

etiqueta_1=Label(gui, text='Resultado') 
etiqueta_1.grid(row=5, column=3)



gui.mainloop()