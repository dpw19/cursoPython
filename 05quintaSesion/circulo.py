# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 12:17:17 2019

@author: PILARES Benita G 26

Funciones (Modulos) quinta sesion
"""

#Crear un módulo con el nombre circulo y almacenar dos funciones

import math

# calculo del area
def area(r):
    a=math.pi*r**2
    return a

# calculo del perímetro
def perimetro(r):
    p=2*math.pi*r
    return p
