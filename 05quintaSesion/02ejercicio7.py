# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 12:53:57 2019

@author: Pavel E. Vazquez Mtz
"""

#from mi_modulo import raices, volumen
import mi_modulo



x = mi_modulo.raices(3, 5, 9)
print("Las raices son:", x)

v = mi_modulo.volumen(5,3)
print("El volumen es:", v)