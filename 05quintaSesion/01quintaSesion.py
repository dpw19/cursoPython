# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 11:13:43 2019

@author: Pavel E. Vázquez
"""

#%% Funciones

#Definicion de la funcion

def mi_funcion():
    ''' Funcion en python que imprime dos mensajes'''
    print('Hola a todos')
    print('Primera función en python')
    
#llamar a la funcion
    
mi_funcion()

#%% Resolver una función matemática

def f(x):
    y = 3 * x**2 + 1
    return y

x = float(input("Escribe el valor de x: "))

variable = f(x)
print(variable)

#%% Funcion de forma abreviada

def f(x): return 3 * x**2 + 1

f(2)

#%% Funcion con más argumentos

def mi_funcion2(cadena,numero):
    ''' Escribe una cadena de texto el numero
    de veces que le asignemos a número '''
    #print(cadena*numero)
    for i in range(numero):
        print(cadena)

mi_funcion2('Algoritmo', 5)

#%% Cuncion que calcula la potencia de un número

def cuadrado(num):
    '''Muestra el cuadrado de un número'''
    return num*num

y = cuadrado(5)

#%% Cuncion que calcula la potencia de un número


def cuadrado2():
    '''Muestra el cuadrado de un número'''
    num = float(input("Ingresa un número: "))
    y=cuadrado(num)
    return y 

variable2 = cuadrado2()

#%% Función que calcule la potencia p de un numero

def potencia(n,p):
    print(n**p)
    
potencia(2,2)

#%% Cuando la funcion se encuentra dentro de un programa

def f(x):
    y = 3 * x**2 + 1
    return y

# Cuerpo del programa 
    
for i in range(5):
    y = f(i)
    print(i,y)
    
#%% Cuando la funcion esta en otro archivo
    
#from circulo import*
from circulo import perimetro, area

r = 5

A = area(r)
print(A)
P = perimetro(r)
print(P)

#%% Funcion que calcula el volumen de un cilindro

import math

def volumen (r, h):
    '''Funcion que calcula el volumenr de un cilindro dados su radio (r)
    y su altura (h)'''
    vol = math.pi*r**2 * h
    return vol
    
volumen(5,2)

#%% Funcion que da solucion a una ecuacion cuadrática


def raices(a, b, c):
    '''Funcion que calcula las ríces de una funcion cuadrática
    dados los coeficientes (a), (b), (c).'''
    x1 = -(b + (b*b - 4*a*c)**(1/2))/(2*a)
    x2 = -(b - (b*b - 4*a*c)**(1/2))/(2*a)
    x = [x1,x2]
    return x

raices(3,4,6)

