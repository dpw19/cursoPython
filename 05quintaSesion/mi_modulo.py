# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 12:51:38 2019

@author: Pavel E Vazquez
"""


# Funcion que calcula el volumen de un cilindro

import math

def volumen (r, h):
    '''Funcion que calcula el volumenr de un cilindro dados su radio (r)
    y su altura (h)'''
    vol = math.pi*r**2 * h
    return vol
    
#volumen(5,2)

# Funcion que da solucion a una ecuacion cuadrática del tipo
# ax^2 + bx +c 

def raices(a, b, c):
    '''Funcion que calcula las ríces de una funcion cuadrática
    dados los coeficientes (a), (b), (c).'''
    x1 = -(b + (b*b - 4*a*c)**(1/2))/(2*a)
    x2 = -(b - (b*b - 4*a*c)**(1/2))/(2*a)
    x = [x1,x2]
    return x

#raices(3,4,6)